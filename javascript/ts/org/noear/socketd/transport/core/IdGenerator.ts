/* Generated from Java with JSweet 3.1.0 - http://www.jsweet.org */
namespace org.noear.socketd.transport.core {
    /**
     * Id 生成器
     * 
     * @author noear
     * @since 2.0
     * @class
     */
    export interface IdGenerator {
        /**
         * 生成
         * @return {string}
         */
        generate(): string;
    }
}

